package com.boot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Sebastian Coteanu on 07.07.2017.
 */

@RestController
public class HomeController {

    @RequestMapping("/")
    public String home() {
        return "First Spring Project : Das-boot";
    }
}
